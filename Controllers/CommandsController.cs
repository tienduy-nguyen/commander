using System.Collections.Generic;
using AutoMapper;
using Commander.Data;
using Commander.Dtos;
using Commander.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Commander.Controllers {

  [Route ("api/[controller]")]
  [ApiController]
  public class CommandsController : ControllerBase {

    private readonly ICommanderRepo _repository;
    private readonly IMapper _mapper;

    public CommandsController (ICommanderRepo repository, IMapper mapper) {
      _repository = repository;
      _mapper = mapper;
    }

    /// <summary>
    /// Get all commands availables
    /// </summary>
    /// <returns>List of commands</returns>
    /// 
    [HttpGet]
    public ActionResult<IEnumerable<Command>> GetAllCommands () {
      var commandItems = _repository.GetAllCommands ();
      return Ok (_mapper.Map<IEnumerable<CommandReadDto>> (commandItems));
    }

    /// <summary>
    /// Get a specific command by id
    /// </summary>
    /// <param name="id"></param>
    /// <returns>A command matching with id given</returns>
    [HttpGet ("{id}", Name = "GetCommandById")]
    [ProducesResponseType (StatusCodes.Status201Created)]
    [ProducesResponseType (StatusCodes.Status400BadRequest)]
    public ActionResult<Command> GetCommandById (int id) {
      var commandItem = _repository.GetCommandById (id);
      if (commandItem != null) {
        return Ok (_mapper.Map<CommandReadDto> (commandItem));
      }
      return NotFound ();

    }

    /// <summary>
    /// Create new command
    /// </summary>
    /// <param name="commandCreateDto"></param>
    /// <returns>A newly created command</returns>
    /// <response code="201">Returns the newly created item</response>
    /// <response code="400">If the item is null</response> 
    [HttpPost]
    [ProducesResponseType (StatusCodes.Status201Created)]
    [ProducesResponseType (StatusCodes.Status400BadRequest)]
    public ActionResult<CommandReadDto> CreateCommand (CommandCreateDto commandCreateDto) {
      var commandModel = _mapper.Map<Command> (commandCreateDto);
      _repository.CreateCommand (commandModel);
      _repository.SaveChanges ();
      var CommandReadDto = _mapper.Map<CommandReadDto> (commandModel);
      return CreatedAtRoute (
        nameof (GetCommandById),
        new { Id = CommandReadDto.Id },
        CommandReadDto
      );

    }

    /// <summary>
    /// Update a command given by Id
    /// </summary>
    /// <param name="id"></param>
    /// <param name="commandUpdateDto"></param>
    /// <returns></returns>
    [HttpPut ("{id}")]
    public ActionResult UpdateCommand (int id, CommandUpdateDto commandUpdateDto) {
      var commandModelFromRepo = _repository.GetCommandById (id);
      if (commandModelFromRepo == null) return NotFound ();
      _mapper.Map (commandUpdateDto, commandModelFromRepo);
      _repository.UpdateCommand (commandModelFromRepo);
      _repository.SaveChanges ();
      return NoContent ();
    }

    /// <summary>
    /// Update a partial of command
    /// </summary>
    /// <param name="id"></param>
    /// <param name="patchDoc"></param>
    /// <returns></returns>
    [HttpPatch ("{id}")]
    public ActionResult PartialCommandUpdate (int id, JsonPatchDocument<CommandUpdateDto> patchDoc) {
      var commandModelFromRepo = _repository.GetCommandById (id);
      if (commandModelFromRepo == null) return NotFound ();
      var commandToPatch = _mapper.Map<CommandUpdateDto> (commandModelFromRepo);
      patchDoc.ApplyTo (commandToPatch, ModelState);

      if (!TryValidateModel (commandToPatch)) {
        return ValidationProblem (ModelState);
      }

      _mapper.Map (commandToPatch, commandModelFromRepo);

      _repository.UpdateCommand (commandModelFromRepo);
      return NoContent ();

    }

    /// <summary>
    /// Delete a command
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete ("{id}")]
    public ActionResult DeleteCommand (int id) {
      var commandModelFromRepo = _repository.GetCommandById (id);
      if (commandModelFromRepo == null) {
        return NotFound ();
      }
      _repository.DeleteCommand (commandModelFromRepo);
      _repository.SaveChanges ();

      return NoContent ();
    }

  }
}